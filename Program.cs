﻿using System;
using Sorters;
using System.Linq;

namespace sorting_algorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("1) Quick Sort\n2) Merge Sort\n3) Heap Sort");
                var sortSelection = Console.ReadKey().KeyChar;
                Console.WriteLine();
                var inputArray = GetArray();

                switch(sortSelection) {
                    case '1': 
                        RunQuickSort(inputArray);
                        break; 
                    case '2': 
                        RunMergeSort(inputArray);
                        break;
                    case '3':
                        RunHeapSort(inputArray);
                        break;
                    default: break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        private static void RunQuickSort(int[] arr)
        {
            Quicksort.Sort(arr);
        }

        private static void RunMergeSort(int[] arr)
        {
            Mergesort.Sort(arr);
        }
        
        private static void RunHeapSort(int[] arr)
        {
            HeapSort.Sort(arr);
        }

        private static void RunBubbleSort(int[] arr)
        {

        }

        private static void RunSelectionSort(int[] arr)
        {

        }

        private static void RunInsertionSort(int[] arr)
        {

        }

        private static int[] GetArray()
        {
            Console.WriteLine("Please enter comma separated integer input:");
            var input = Console.ReadLine();
            var output = input.Split(',').Select(int.Parse).ToArray();
            return output;
        }
    }
}
