using System;

namespace Common
{
    public class SortingHelper
    {
        public static void Swap(int[] a, int x, int y)
        {
            var temp = a[x];
            a[x] = a[y];
            a[y] = temp;
        }

        public static void PrintSubArray(int[] arr, int firstIndex, int lastIndex)
        {
            while(firstIndex < lastIndex)
            {
                Console.Write(arr[firstIndex]);
                if (firstIndex < lastIndex - 1)
                {
                    Console.Write(", ");
                }
                firstIndex++;
            }

            Console.WriteLine();
        }
    }
}