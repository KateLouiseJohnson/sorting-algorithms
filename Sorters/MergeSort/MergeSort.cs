using System;
using Common;

namespace Sorters
{
    public class Mergesort
    {
        public static void Sort(int[] arr)
        {
            SortingHelper.PrintSubArray(arr, 0, arr.Length);
            var sortedArray = GetSorted(arr);
            SortingHelper.PrintSubArray(sortedArray, 0, sortedArray.Length);
        }

        private static int[] GetSorted(int[] arr)
        {
            if(arr.Length <= 1) return arr;
            
            var splitPoint = arr.Length/2;
            var secondArrLength = arr.Length - splitPoint;
            var firstHalf = new int[splitPoint];
            var secondHalf = new int[secondArrLength];
            CopySubArray(arr, 0, firstHalf, 0, firstHalf.Length);
            CopySubArray(arr, splitPoint, secondHalf, 0, secondArrLength);
            var sortedA = GetSorted(firstHalf);
            var sortedB = GetSorted(secondHalf);

            return Merge(sortedA,sortedB);
        }

        private static int[] Merge(int[] arrA, int[] arrB)
        {
            var arrC = new int[arrA.Length + arrB.Length];
            var aIndex = 0;
            var bIndex = 0;
            var cIndex = 0;

            while (aIndex < arrA.Length && bIndex < arrB.Length)
            {
                if (arrA[aIndex] < arrB[bIndex])
                {
                    arrC[cIndex] = arrA[aIndex];
                    cIndex++;
                    aIndex++;
                } 
                else
                {
                    arrC[cIndex] = arrB[bIndex];
                    cIndex++;
                    bIndex++;                    
                }
            }

            for ( ; aIndex < arrA.Length; aIndex++, cIndex++)
            {
                arrC[cIndex] = arrA[aIndex];
            }

            for ( ; bIndex < arrB.Length; bIndex++, cIndex++)
            {
                arrC[cIndex] = arrB[bIndex];
            }

            return arrC;
        }
    
        private static void CopySubArray(int[] fromArr, int fromStartIndex, int[] toArr, int toStartIndex, int toEndIndex)
        {
            for ( ; toStartIndex < toEndIndex; fromStartIndex++, toStartIndex++) 
            {
                toArr[toStartIndex] = fromArr[fromStartIndex];
            }
        }
    }
}