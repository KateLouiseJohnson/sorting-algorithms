using System;
using Common;

namespace Sorters
{
    public class HeapSort
    {
        public static void Sort(int[] arr)
        {
            SortingHelper.PrintSubArray(arr, 0, arr.Length);
            
            Heapify(arr, arr.Length);
            
            for (var i = arr.Length - 1; i > 0; i--)
            {
                var val = arr[i];
                arr[i] = arr[0];
                arr[0] = val;
                SiftDown(arr, i-1, 0);
            }
            
            Console.WriteLine("Sorting complete.");
            SortingHelper.PrintSubArray(arr, 0, arr.Length);
        }

        private static void Heapify(int[] arr, int len)
        {
            // Start working from the last parent, which will be the parent of the last element            
            for(var i = GetParentIndex(len); i >= 0; i--)
            {
                SiftDown(arr, len, i);               
            }
        }

        private static int GetParentIndex(int childIndex)
        {
            return (childIndex - 1)/2;
        }

        private static int GetLeftChildIndex(int parentIndex)
        {
            return 2 * parentIndex + 1;
        }
        
        private static int GetRightChildIndex(int parentIndex)
        {
            return 2 * parentIndex + 2;
        }

        private static int GetGreaterValueIndex(int[] arr, int leftIndex, int rightIndex)
        {
            if(arr[leftIndex] > arr[rightIndex])
            {
                return leftIndex;
            }
            return rightIndex;
        }

        private static void SiftDown(int[] arr, int len, int targetIndex)
        {
            var l = GetLeftChildIndex(targetIndex);
            if (l > len) return;
            
            var r = GetRightChildIndex(targetIndex);
            var greatestChildIndex = r < len ? GetGreaterValueIndex(arr, l, r) : l;

            if (arr[greatestChildIndex] > arr[targetIndex])
            {
                SortingHelper.Swap(arr, greatestChildIndex, targetIndex);
                // Now that we have moved a smaller value downards we want to keep sifting it down
                SiftDown(arr, len, greatestChildIndex);
            }
        }

        private static void SiftUp(int[] arr, int len, int index)
        {
            if (index == 0) return;
            var parent = GetParentIndex(index);

            if (arr[parent] < arr[index])
            {
                SortingHelper.Swap(arr, parent, index);
                SiftUp(arr, len, parent);
            }
        }
    }
}