using System;
using Common;

namespace Sorters
{
    public static class Quicksort
    {
        public static void Sort(int[] arr)
        {
            SortingHelper.PrintSubArray(arr, 0, arr.Length);
            Sort(arr, 0, arr.Length);
            SortingHelper.PrintSubArray(arr, 0, arr.Length);
            Console.WriteLine("Sorting complete.");
        }

        private static void Sort(int[] arr, int startIndex, int endIndex)
        {
            if(startIndex == endIndex) return;
            var firstEqualIndex = startIndex;
            var firstGreaterThanIndex = endIndex;
            
            // Divide into greater and lower values
            Partition(arr, ref firstEqualIndex, ref firstGreaterThanIndex);
            // Sort lower values
            Sort(arr, startIndex, firstEqualIndex);
            // Sort higher values
            Sort(arr, firstGreaterThanIndex, endIndex);          
        }

        private static void Partition(int[] a, ref int start, ref int firstGreaterThan)
        {
            var pivot = a[start];
            var nextIndex = start + 1;

            while (nextIndex < firstGreaterThan)
            {
                var next = a[nextIndex];
                if (next < pivot)
                {
                    // Move to Less Than section
                    SortingHelper.Swap(a, start, nextIndex);
                    start++;
                } 
                else if (next > pivot)
                {
                    // Move to Greater Than section
                    firstGreaterThan--;
                    SortingHelper.Swap(a, firstGreaterThan, nextIndex);
                } 
                else
                {
                    // Adjust Equal To section
                    nextIndex++;
                }
            }
        }

    }
}